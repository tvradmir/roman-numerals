package org.vlado.romannumerals;

import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * @author Vlado on 10/09/2021
 * @project roman-numerals
 */
public class RomanNumerals {

    private final NavigableMap<Integer,String> arabicToRomanNumeral = buildArabicToRomanNumerals();

    public String romanValueOf(Integer arabicValue) {

        var romanBuilder = new StringBuilder();

        var floorEntry = arabicToRomanNumeral.floorEntry(arabicValue);

        if ( arabicValue.equals(floorEntry.getKey()) ) {
            return floorEntry.getValue();
        }

        romanBuilder.append(floorEntry.getValue());

        return romanBuilder.append(romanValueOf(arabicValue - floorEntry.getKey())).toString();
    }

    public Integer arabicValueOf(String romanValue) {
        int result=0;

        var sb = new StringBuilder(romanValue);

        for (var entry:arabicToRomanNumeral.descendingMap().entrySet()) {
            while (sb.indexOf(entry.getValue())==0){
                result+=entry.getKey();
                sb.delete(0,entry.getValue().length());
            }

        }
        return result;
    }


    private TreeMap<Integer, String> buildArabicToRomanNumerals() {
        return new TreeMap<>() {
            {
                put(1, "I");
                put(4, "IV");
                put(5, "V");
                put(9, "IX");
                put(10, "X");
                put(40, "XL");
                put(50, "L");
                put(90, "XC");
                put(100, "C");
                put(400, "CD");
                put(500, "D");
                put(900, "CM");
                put(1000, "M");
            }
        };
    }

}
