package org.vlado.romannumerals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Vlado on 10/09/2021
 * @project roman-numerals
 */
public class RomanNumeralsTest {


    private RomanNumerals romanNumerals;

    @BeforeEach
    void setUp() {
        romanNumerals = new RomanNumerals();
    }

    @ParameterizedTest(name = "roman value of {0} should be {1}")
    @CsvSource(value = {
            "1:I", "2:II", "3:III",
            "5:V","6:VI","8:VIII",
            "10:X","12:XII","15:XV",
            "20:XX","30:XXX",
            "50:L","100:C","500:D",
            "1000:M",
            "4:IV","9:IX","40:XL",
            "90:XC","400:CD","900:CM",
            "3999:MMMCMXCIX"
    }, delimiter = ':')
    void should_return_romanValue_of_given_arabic_value(Integer arabicValue, String expected) {
        assertThat(romanNumerals.romanValueOf(arabicValue)).isEqualTo(expected);
    }

    @ParameterizedTest(name = "arabic value of {0} should be {1}")
    @CsvSource(value = {
            "I:1","II:2","III:3",
            "V:5","VI:6","VII:7",
            "X:10","XI:11","XIII:13","XV:15","XVIII:18",
            "L:50","LII:52","LV:55",
            "XX:20","XXX:30",
            "C:100","D:500","M:1000",
            "IV:4","LIV:54","IX:9","XL:40",
            "XC:90","CD:400","CM:900",
            "CLIX:159","XXXIV:34","XXXVII:37",
            "XLIII:43","XLVIII:48",
            "CXL:140","CXLV:145"
    }, delimiter = ':')
    void should_return_arabicValue_of_given_romanValue(String romanValue, Integer expected) {
        assertThat(romanNumerals.arabicValueOf(romanValue)).isEqualTo(expected);
    }


}
